class User < ActiveRecord::Base
  attr_accessor :remember_token
  before_save { self.email = email.downcase }
  
  # for validation attributes
  validates :name, presence:true, length:{maximum: 50}
  validates :email, presence:true, length:{maximum: 250}
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, format: { with: VALID_EMAIL_REGEX }, uniqueness: true, case_sensitive:false
  validates :password, length: { minimum: 6}, allow_blank: true
  
  # indicate that user model will use secure password
  has_secure_password
  
  # return the hash digest of given string
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end
  
  # return a random token
  def User.new_token
    SecureRandom.urlsafe_base64
  end
  
  # remembers a user in the database for use in persistent sessions
  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end
  
  # returns true if the given token matches the digest
  def authenticated?(remember_token)
    return false if remember_digest.nil?
    BCrypt::Password.new(remember_digest).is_password?(remember_token)
  end
  
  # forget a user
  def forget
    update_attribute(:remember_digest, nil)
  end
end
